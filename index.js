// @flow

const inquirer = require( 'inquirer' );
const Manner = require( 'manner-sdk-js' );
const Rx = require( 'rx' );
const Promise = require( 'bluebird' );

const questions = require( './questions.js' );

Manner.setToken( 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyfQ.12qsPir23tIQn6_FuRewfc-VhbE9LlE2155OWrEBHkU' );
Manner.setAgentID( 'QWdlbnQ6Mw' );
Manner.setDebug( true )

const prompts = new Rx.Subject();
const regularFlow = {
	// start point
	start: 'toBeDelivered',
	// after toBeDelivered, we now run a Manner test instead of specifying order
	// toBeDelivered: 'size',
	toBeDelivered: 'size',
	pizzaOrder: 'beverage',
	beverage: 'comments',
	comments: 'prize',
	size: 'quantity',
	quantity: 'toppings',
	toppings: 'beverage',
};

let currentTest = null;

/*
 * Function Definitions
 ************************/

// determine the next response to take according to regular conversation flow
function getResponse( lastAnswer, userId ) {

	let action;
	switch ( lastAnswer.name ) {

		case 'toBeDelivered':
			// Here we want to run an action A/B test, so let's invoke Manner and see what to do
			// NB this will automatically resolve the action directly into a response
			return Manner.getTestResponse( userId, 'pizzaOrder' )
				.then(result =>
					({
						type: 'input',
						name: result.action,
						message: result.response
					})
				);

		// in all other cases, we are going to use Manner to automatically A/B test different responses
		// for each action
		case 'comments':
			// end the conversation if no comment is left
			if ( lastAnswer.answer.length > 0 ) {
				action = regularFlow[ lastAnswer.name ];
			} else {
				return Promise.resolve( null );
			}
		case 'prize':
			return Promise.resolve( null );
		default:
			action = ( regularFlow[ lastAnswer.name ] );
	}

	const normalResponse = questions[action].message;
	return Manner.getResponse( userId, action, normalResponse )
		.then(mannerResponse =>
			({
				type: 'input',
				name: action,
				message: mannerResponse
			})
		);
}

// function getQuestion( action, conversation ) {
// 	if ( action === null ) return null;
//
// 	// always use Manner's suggestions
// 	// (it will fall back to the default if necessary)
// 	const fallback = new Manner.Response( Manner.getConfig, action, questions[ action ].message, {
// 		quickReplies: questions[ action ].choices
// 	} );
//
// 	return conversation.getAction( action ).invoke( [], fallback )
// 		// convert Manner responses into inquirer questions
// 		.then( ( response ) => {
// 			switch ( response.quickReplies.length ) {
// 				case 0:
// 					return {
// 						type: 'input',
// 						name: action,
// 						message: response.value
// 					};
// 				case 1:
// 					return {
// 						type: 'input',
// 						name: action,
// 						default: response.quickReplies[ 0 ],
// 						message: response.value
// 					};
// 				default:
// 					return {
// 						type: 'list',
// 						name: action,
// 						choices: response.quickReplies,
// 						message: response.value
// 					};
// 			}
// 		} );
// }

function main() {

	// construct prompt
	const p = inquirer.prompt( prompts );

	// setup Manner objects
	// here we are using emails as user IDs
	const userId = Math.floor(Math.random()*1000) + '@manner.ai'
	// const conversation = Manner.startConversation( userId );
	Manner.startGoal( userId, 'pizzaOrder' );
	Manner.startGoal( userId, 'leaveComment' );

	Manner.context(userId, "channel", ["Slack", "Facebook", "Telegram", "Kik"][Math.floor(Math.random()*4)])
	Manner.context(userId, "location", ["London", "Paris", "Cardiff", "Cambridge"][Math.floor(Math.random()*4)])
	Manner.userContext(userId, "gender", ["male", "female"][Math.floor(Math.random()*2)])
	Manner.userContext(userId, "age", Math.floor(Math.random()*80))

	const say =  question => {
		if (question) {
			Manner.logOutgoing( userId, question )
			prompts.onNext( question )
		} else {
			prompts.onCompleted()
		}
	}

	// attach appropriate listeners to handle event after each message
	p.ui.process.subscribe(
		( lastAnswer ) => {
			// Let Manner know what the user is saying
			Manner.logIncoming( userId, lastAnswer.answer );

			// log some extra user context
			switch ( lastAnswer.name ) {
				case "toBeDelivered":
					Manner.userContext( userId, "wantsDelivery", lastAnswer.answer === "yes" );
					break;
				case "beverage":
					Manner.userContext( userId, "favourite-drink", lastAnswer.answer );
					break;
				case "toppings":
					Manner.userContext( userId, "favourite-pizza", lastAnswer.answer );
					break;
				case "comments":
					if (lastAnswer.answer.length > 0) {
						Manner.goalSuccess(userId, "leaveComment")
					} else {
						Manner.goalFail(userId, "leaveComment")
					}
					break;
			}

			getResponse( lastAnswer, userId )
				.then( say );
		},

		// error handler
		(e) => {
			console.error( 'processing Error' );
			console.error(e)
			Manner.goalFail( userId, "pizzaOrder" )
			Manner.goalFail( userId, "leaveComment" )
		}
	);

	// attach a handler for when the conversation is over
	p.then( ( allAnswers ) => {
		Manner.goalSuccess( userId, "pizzaOrder" )
	} );

	// finally start the conversation
	console.log( 'Hi, welcome to Node Pizza' );

	getResponse( { name: 'start', answer: '' }, userId )
		.then( say );
}


// kick things off
main();
